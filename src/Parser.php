<?php

namespace BFMD;

class Parser
{
    /**
     * @param $text string
     *
     * @return string
     */
    public function parseToHTML($text)
    {
        $text = preg_replace('{^\xEF\xBB\xBF|\x1A}', '', $text);
        $text = preg_replace('{\r\n?}', "\n", $text);
        $text = preg_replace('/\n\n/', "\n\n\n", $text);

        $text = "\n".$text."\n\n";

        $text = preg_replace_callback('/\n(#{1,2})(.*)/', 'self::header', $text);
        $text = preg_replace('/\[([^\[]+)\]\(([^\)]+)\)/', '<a href="\2">\1</a>', $text);
        $text = preg_replace_callback('/\n(.*?(\n).*?)\n\n/', 'self::paragraph', $text);
        $text = preg_replace('/___EAT___/', '<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Broccoli_and_cross_section_edit.jpg/320px-Broccoli_and_cross_section_edit.jpg" title="Broccoli is yummy!" alt="A lovely picture of broccoli" />', $text);

        return trim($text);
    }

    /**
     * @param $regs array
     *
     * @return string
     */
    private function header($regs)
    {
        list($tmp, $chars, $header) = $regs;
        $level = strlen($chars);

        return sprintf("\n<h%d>%s</h%d>\n", $level, trim($header), $level);
    }

    /**
     * @param $regs array
     *
     * @return string
     */
    private function paragraph($regs)
    {
        $line = $regs[1];
        $trimmed = trim($line);
        if (preg_match('/^<\/?(ul|ol|li|h|p|bl)/', $trimmed)) {
            return "\n".$line."\n";
        }

        return sprintf("\n<p>%s</p>\n", $trimmed);
    }
}
