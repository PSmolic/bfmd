<?php

namespace BFMD\tests;

use BFMD\Parser;

class ParserTest extends \PHPUnit_Framework_TestCase
{
    public function testCanConstruct()
    {
        $parser = new Parser();

        $this->assertInstanceOf('BFMD\Parser', $parser);
    }

    public function testHeaders()
    {
        $parser = new Parser();

        $h1 = $parser->parseToHTML('# Header 1');
        $h2 = $parser->parseToHTML('## Header 2');

        $this->assertEquals($h1, '<h1>Header 1</h1>');
        $this->assertEquals($h2, '<h2>Header 2</h2>');
    }

    public function testLinks()
    {
        $parser = new Parser();

        $link = $parser->parseToHTML('[Title](url)');

        $this->assertEquals($link, '<a href="url">Title</a>');
    }

    public function testParagraph()
    {
        $parser = new Parser();

        //this would fail because regex for paragraphs isn't good enough
        //$oneLineParagraph = $parser->parseToHTML('one line');
        //$this->assertEquals($oneLineParagraph, '<p>one line</p>');

        $paragraph = $parser->parseToHTML('this is my testing paragraph
still first

This is second paragraph
Still second');

        $this->assertEquals($paragraph, '<p>this is my testing paragraph
still first</p>

<p>This is second paragraph
Still second</p>');
    }

    public function testEat()
    {
        $parser = new Parser();

        $link = $parser->parseToHTML('___EAT___');

        $this->assertEquals($link, '<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Broccoli_and_cross_section_edit.jpg/320px-Broccoli_and_cross_section_edit.jpg" title="Broccoli is yummy!" alt="A lovely picture of broccoli" />');
    }

    public function testCombined()
    {
        $parser = new Parser();

        $link = $parser->parseToHTML('# Broccoli
Broccoli is an edible green plant in the cabbage family whose large flowerhead is eaten as a vegetable.

## Etymology
The word broccoli comes from the Italian plural of broccolo, which means "the flowering crest of a cabbage", and is the diminutive form of brocco, meaning "small nail" or "sprout". Broccoli is often boiled or steamed but may be eaten raw.

Having this said, you can read up on it even more [here](https://en.wikipedia.org/wiki/Broccoli). Also be sure to checkout this wonderful picture of one.
___EAT___');

        $expected = '<h1>Broccoli</h1>
<p>Broccoli is an edible green plant in the cabbage family whose large flowerhead is eaten as a vegetable.</p>

<h2>Etymology</h2>
<p>The word broccoli comes from the Italian plural of broccolo, which means "the flowering crest of a cabbage", and is the diminutive form of brocco, meaning "small nail" or "sprout". Broccoli is often boiled or steamed but may be eaten raw.</p>

<p>Having this said, you can read up on it even more <a href="https://en.wikipedia.org/wiki/Broccoli">here</a>. Also be sure to checkout this wonderful picture of one.
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Broccoli_and_cross_section_edit.jpg/320px-Broccoli_and_cross_section_edit.jpg" title="Broccoli is yummy!" alt="A lovely picture of broccoli" /></p>';

        $this->assertEquals($link, $expected);
    }
}
