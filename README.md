# Usage

```$ composer clearcache```
```$ composer require psmolic/bfmd```


```
use BFMD\Parser
...

$parser = new Parser();
$string = $parser->parseToHTML($markdownString);

```

# Task:

Our main dish is a Parser for BFMD (Broccoli flavored MarkDown).
The task is to create a parser that takes a given input and converts it to some output using some of the standard MarkDown components and some special flavor.

That said, BFMD has the following components:

The paragraph behaviour is the same as in the normal MarkDown (Read up on it here)
[linked text](URL) that will be converted to <a href="URL">linked text</a>
# Some text that will be converted to <h1>Some text</h1>
## Some text #2 that will be converted to <h2>Some text #2</h2>
___EAT___ that will be converted to <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Broccoli_and_cross_section_edit.jpg/320px-Broccoli_and_cross_section_edit.jpg" title="Broccoli is yummy!" alt="A lovely picture of broccoli" />
Just take this example input to test your parser.

INPUT:
```
# Broccoli
Broccoli is an edible green plant in the cabbage family whose large flowerhead is eaten as a vegetable.

## Etymology
The word broccoli comes from the Italian plural of broccolo, which means "the flowering crest of a cabbage", and is the diminutive form of brocco, meaning "small nail" or "sprout". Broccoli is often boiled or steamed but may be eaten raw.

Having this said, you can read up on it even more [here](https://en.wikipedia.org/wiki/Broccoli). Also be sure to checkout this wonderful picture of one.
___EAT___
```
OUTPUT:
```
<h1>Broccoli</h1>
<p>Broccoli is an edible green plant in the cabbage family whose large flowerhead is eaten as a vegetable.</p>

<h2>Etymology</h2>
<p>The word broccoli comes from the Italian plural of broccolo, which means "the flowering crest of a cabbage", and is the diminutive form of brocco, meaning "small nail" or "sprout". Broccoli is often boiled or steamed but may be eaten raw.</p>

<p>Having this said, you can read up on it even more <a href="https://en.wikipedia.org/wiki/Broccoli">here</a>. Also be sure to checkout this wonderful picture of one.
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Broccoli_and_cross_section_edit.jpg/320px-Broccoli_and_cross_section_edit.jpg" title="Broccoli is yummy!" alt="A lovely picture of broccoli" /></p>
Your settings are as follows:
```
You will have to create a composer package
You can use PHP features up to version 5.5
Your file/folder structure needs to follow the PSR-4 standard, with a base namespace of BFMD
Optional: You use packagist to distribute your BFMD-Parser, so we could easily use it in our project
